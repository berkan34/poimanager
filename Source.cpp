﻿





#include<map>
#include <set>
#include <list>
#include <iostream>
#include <string>
#include<vector>
#include<array>
#include<algorithm>
#include <stdio.h>
#include <locale>
#include <codecvt>
#include<unordered_map>
using namespace std;


static class Poi{

private:

	int level;

	string type;

	int facilityId;

	string name;

	string description;

	list<string> keyword;

public:
	string getType() {return this->type;}

	string getSearchType() { return this->type; }

	int getFloor() { return this->level; }

	int getFacilityId() { return this->facilityId; }

	string getSearchName() { return this->name; }

	string getSearchDescription() { return this->description; }

	list<string> getSearchKeywords() { return this->keyword; }

	bool operator == (const Poi& s) const { return name == s.name && facilityId == s.facilityId; }
	bool operator != (const Poi& s) const { return !operator==(s); }

	Poi(int level, string type, int facilityId, string description) { this->type = type; this->level = level; this->facilityId = facilityId; this->description = description; }



};

class PoiContainer {

public:

	map<string, list<Poi>>container;


	PoiContainer(map<string, list<Poi>> container) { this->container = container; }

	PoiContainer() { }



};


class Venue {



};


class PoiManagerImpl {

public:
	
	PoiContainer selectedPoi;
	list<Poi> allPois;


	map<string, list<Poi>> groupedPois; 
	

	list<Poi> triggerPois;
	list<Poi> storeLayoutPois;
	//Storage localDataStorage;
	//CoreConfiguration configuration;


private:



	string TAG = "PoiManagerImpl";

 /*   final Object positionLock;
	final Object triggerPoisLock;

	Position prevPosition;

	Position *ptr = &prevPosition;

	ptr = NULL;

	*/

	map<Poi, bool> isInThePOI;

/*	void resetVariables() {
		resetCache();
		this->resetSelectedPoi();
	}

	void resetCache() {
		list<Poi> *p = &allPois;
		p = NULL;
		list<Poi> *p = &allPois;
		p= NULL;

	}
	*/
public:

	

	PoiManagerImpl() {


	}

	/*@Override
		void start() {
		DataManager dataManager = Pointr.getPointr().getDataManager();
		DataManager *ptr = &dataManager;  //RENAME ptr's
		if (ptr != NULL) {
			dataManager.addListener(this);
		}
		else {
			Log.w(TAG, "Cannot bind to Data Manager, because it was null");
		}

		ConfigurationManager configManager = Pointr.getPointr().getConfigurationManager();
		ConfigurationManager *ptr;
		if (ptr != NULL) {
			configManager.addListener(this);
		}
		else {
			Log.w(TAG, "Cannot bind to Configuration Manager, because it was null");
		}

		PositionManager positionManager = Pointr.getPointr().getPositionManager();
		PositionManager *ptr;
		if (ptr != NULL) {
			positionManager.addListener(this);
		}
		else {
			Log.w(TAG, "Cannot bind to Position Manager, because it was null");
		}
	}


	@Override
		void stop() {
		DataManager dataManager = Pointr.getPointr().getDataManager();
		DataManager *ptr = &dataManager;
		if (dataManager != NULL) {
			dataManager.removeListener(this);
		}
		else {
			Log.w(TAG, "Couldn't remove listener from Data Manager, because it was null");
		}

		ConfigurationManager configManager = Pointr.getPointr().getConfigurationManager();
		ConfigurationManager *ptr = &configManager;
		if (ptr != NULL) {
			configManager.removeListener(this);
		}
		else {
			Log.w(TAG, "Couldn't remove listener from Configuration Manager, because it was null");
		}

		PositionManager positionManager = Pointr.getPointr().getPositionManager();
		PositionManager *ptr = &positionManager;
		if (ptr != NULL) {
			positionManager.removeListener(this);
		}
		else {
			Log.w(TAG, "Couldn't remove listener from Position Manager, because it was null");
		}

		resetVariables();
	}*/




	//poi methods----- getPoi

	void printPoiList(PoiContainer cont) {   //printer method

		map<string, list<Poi>> pcontainer = cont.container;

		map<string, list<Poi>>::iterator itr;

		for (itr = pcontainer.begin(); itr != pcontainer.end(); itr++) {

			string key = itr->first;

			list<Poi> poilist = itr->second;

			for (Poi poi : poilist) {

				cout << poi.getFloor() << "  " << poi.getType() << "  " << poi.getFacilityId() << endl;

			}

		}

	}



	PoiContainer getAllPoi() {

		

	
		
		
		map<string, list<Poi>> *groupedPoisptr = &groupedPois;

		if (groupedPoisptr == NULL || groupedPois.empty()) {

			throw exception();

		}



		map<string, list<Poi>> newPoiCollection;
		map<string, list<Poi>> poiCollection = groupedPois;

		map<string, list<Poi>>::iterator itr;

		for (itr = poiCollection.begin(); itr != poiCollection.end(); itr++) {

			string key = itr->first;

			list<Poi> value = itr->second;

			list<Poi> newValue;

			for (Poi poi : value) {

				string poiType = poi.getType();

				if (poiType == "displayarea" || poiType == "trigger" || poiType == "externaltrigger") {

					continue;

				}

				newValue.push_back(poi);


			}

			if (newValue.size() > 0) {

				newPoiCollection.insert(pair<string, list<Poi>>(key, newValue));

			}


		}

		return PoiContainer(newPoiCollection);

	}



	PoiContainer getAllPoi(vector<string> poiTypes) {

		vector <string> poiTypeArray = poiTypes;

		vector <string> *ptr = &poiTypeArray;

		map<string, list<Poi>> newPoiCollection;

		if (ptr != NULL) {

			map<string, list<Poi>> *groupedPoisptr = &groupedPois;

			if (groupedPoisptr == NULL || groupedPois.empty()) {

				throw exception();

			}

			map<string, list<Poi>> poiCollection = groupedPois;

			map<string, list<Poi>>::iterator itr;

			for (string type : poiTypeArray) {

				string processedType = type;

				for (itr = poiCollection.begin(); itr != poiCollection.end(); itr++) {

					string key = itr->first;

					list<Poi> value = itr->second;

					list<Poi> newValue;

					for (Poi poi : value) {

						string poiTypeOfSelected = poi.getType();

						if (poiTypeOfSelected == "displayarea" || poiTypeOfSelected == "trigger" || poiTypeOfSelected == "externaltrigger") {

							continue;

						}


						if (poiTypeOfSelected == processedType ) {

							newValue.push_back(poi);

						}



					}

			
					if (newValue.size() > 0) {

						newPoiCollection.insert(pair<string, list<Poi>>(key, newValue));

					}


					

					
				}
			}

		}
		else {
		
			throw exception();
		}





		return PoiContainer(newPoiCollection);


	}


		PoiContainer getAllPoi(int level, string type, int facilityId) {

			map<string, list<Poi>> *groupedPoisptr = &groupedPois;

			if (groupedPoisptr == NULL || groupedPois.empty()) {

				throw exception();

			}



			map<string, list<Poi>> newPoiCollection;
			map<string, list<Poi>> poiCollection = groupedPois;

			map<string, list<Poi>>::iterator itr;
			for (itr = poiCollection.begin(); itr != poiCollection.end(); itr++) {
			


				string key = itr->first;

				list<Poi> value = itr->second;

				list<Poi> newValue;

				for (Poi poi : value) {

					string poiType = poi.getType();
					
					if (poiType == "displayarea" || poiType == "trigger" || poiType == "externaltrigger") {

						continue;

					}

					if (poi.getFloor() == level && poi.getFacilityId() == facilityId && poiType == type ) {

						newValue.push_back(poi);

					}
					
					


				}

				if (newValue.size() > 0) {

					newPoiCollection.insert(pair<string, list<Poi>>(key, newValue));

				}


			}

			

			return PoiContainer(newPoiCollection);

		}
	



		map<string, list<Poi>> getGroupedPoi(Venue venue) {

			
			//	list<Poi> allPois = localDataStorage.getPoiList(venue);
			list<Poi> allPois;
			map<string, list<Poi>> map;

			for (Poi poiData : allPois) {
				list<Poi> poiList;
				string name = poiData.getSearchName();
				string *ptr = &name;
				if (ptr != NULL) {
					string key = name + "\t" + poiData.getType();
					if (!(map.count(key) > 0)) {
						poiList = map.find(key)->second;
					}

					poiList.push_back(poiData);
					map.insert(pair<string, list<Poi>>(key, poiList));

				}
			
			}

				return map;


		}
		
		PoiContainer searchPoi(string searchString) {


			map<string, list<Poi>> *groupedPoisptr = &groupedPois;

			if (groupedPoisptr == NULL || groupedPois.empty()) {

				throw exception();

			}

			map <string, list<Poi>> filteredPoiCollection;


			vector<string> searchTokens = createSearchTokens(searchString);

			//This will keep the first members of groups that pass the search filter.
			//This is for ranking the groups between themselves.
			//It was used to take first members of all groups, filter and rank them
			//This version is more suitable with ios.

			list<Poi> firstPoiInAFilteredGoup;
			
			map<string, list<Poi>> filteredOrderedPoiCollection;

			map<string, list<Poi>>::iterator itr;

			for (itr = groupedPois.begin(); itr != groupedPois.end(); itr++) {

				string key = itr->first;

				list<Poi> value = itr->second;

				list<Poi> filteredPoiList = filterPoi(value, searchTokens);

				list<Poi> *ptr = &filteredPoiList;
				if (ptr == NULL || filteredPoiList.empty()) {

					continue;

				}

				filteredPoiCollection.insert(pair<string, list<Poi>>(key,filteredPoiList));

				firstPoiInAFilteredGoup.push_back(filteredPoiList.front());

			}


			list<Poi> orderedFirstPois = filterPoi(firstPoiInAFilteredGoup, searchTokens);

			for (Poi poi : orderedFirstPois) {


				string name = poi.getSearchName();
				string *ptr = &name;
				if (ptr != NULL) {
					string key = name + "\t" + poi.getType();

					filteredOrderedPoiCollection.insert(pair<string, list<Poi>>(key, filteredOrderedPoiCollection.find(key)->second));
				}
			}



		
		
			return PoiContainer(filteredOrderedPoiCollection);
		}
		
	




		vector<string> createSearchTokens(string searchString) {

			string original = searchString;

			searchFriendlyString(original);
			
			replace(original.begin(), original.end(), ' ', ',');

			vector<string> list = split(original);

			if (list.size() > 1) {

				for (int i = 1; i < list.size(); i++) {

					if (list[i].size() < 3) {

						list.erase(list.begin() + i);

					}
				}

				sort(list.begin(), list.end());
			}

			return list;

		}

		

		vector<string> split(string str) {

			vector<string> v;

			int counter = 0;

			int index = 0;

			for (int i = 1; i < str.size(); i++) {

				if (str.at(i) == '-' || str.at(i) == ',' || str.at(i) == '.') {

					string result = str.substr(index, i - index);

					index = i + 1;

					v.push_back(result);

				}

				if (i == str.size() - 1) {

					string result = str.substr(index, i - index + 1);

					v.push_back(result);


				}


			}

			return v;

		}


		string searchFriendlyString(const std::string& str) {
			if (str.size() == 0) {
				return str;
			}

			unordered_map<wchar_t, char> translationMap = {
				{ L'à','a' },{ L'á','a' },{ L'â','a' },{ L'ä','a' },{ L'æ','a' },{ L'ã','a' },{ L'å','a' },{ L'ā','a' },
				{ L'À','a' },{ L'Á','a' },{ L'Â','a' },{ L'Ä','a' },{ L'Æ','a' },{ L'Ã','a' },{ L'Å','a' },{ L'Ā','a' },
				{ L'ç','c' },{ L'ć','c' },{ L'č','c' },{ L'Ç','c' },{ L'Ć','c' },{ L'Č','c' },
				{ L'è','e' },{ L'é','e' },{ L'ê','e' },{ L'ë','e' },{ L'ē','e' },{ L'ė','e' },{ L'ę','e' },
				{ L'È','e' },{ L'É','e' },{ L'Ê','e' },{ L'Ë','e' },{ L'Ē','e' },{ L'Ė','e' },{ L'Ę','e' },
				{ L'î','i' },{ L'ï','i' },{ L'í','i' },{ L'ī','i' },{ L'į','i' },{ L'ì','i' },{ L'ı','i' },
				{ L'Î','i' },{ L'Ï','i' },{ L'Í','i' },{ L'Ī','i' },{ L'Į','i' },{ L'Ì','i' },{ L'I','i' },
				{ L'ł','l' },{ L'Ł','l' },
				{ L'ñ','n' },{ L'ń','n' },{ L'Ñ','n' },{ L'Ń','n' },
				{ L'ô','o' },{ L'ö','o' },{ L'ò','o' },{ L'ó','o' },{ L'œ','o' },{ L'ø','o' },{ L'ō','o' },
				{ L'õ','o' },{ L'Ô','o' },{ L'Ö','o' },{ L'Ò','o' },{ L'Ó','o' },{ L'Œ','o' },{ L'Ø','o' },{ L'Ō','o' },{ L'Õ','o' },
				{ L'ß','s' },{ L'ś','s' },{ L'š','s' },{ L'Ś','s' },{ L'Š','s' },
				{ L'û','u' },{ L'ü','u' },{ L'ù','u' },{ L'ú','u' },{ L'ū','u' },{ L'Û','u' },{ L'Ü','u' },{ L'Ù','u' },{ L'U','u' },{ L'Ū','u' },
				{ L'ÿ','y' },{ L'Ÿ','y' },
				{ L'ž','z' },{ L'ź','z' },{ L'ž','z' },{ L'Ž','z' },{ L'Ź','z' },{ L'Ż','z' },
				{ L'ğ','g' },{ L'Ğ','g' },
			};


			std::string result;

			// Convert utf8 to wstring (ucs4)
			std::wstring_convert<std::codecvt_utf8<wchar_t>> converter;
			std::wstring wstr = converter.from_bytes(str);

			bool inWhitespace = true;//if inWhitespace skips further whitespace, set to true to ignore whitespace at the begining of the string
			for (auto wch : wstr) {
				auto it = translationMap.find(wch);
				if (it != translationMap.end()) {//char is in replace map
					result += it->second;
					inWhitespace = false;
				}
				else if (isspace(wch) && !inWhitespace) {//handle whitespace
					result += ' ';
					inWhitespace = true;
				}
				else if ((wch >= 'a' && wch <'z') || (wch >= 'A' && wch <'Z')) {//handle alpha
					result += tolower((char)wch);
					inWhitespace = false;
				}
				else if (wch >= '0' && wch <= '9') {//handle numeric
					result += (char)wch;
					inWhitespace = false;
				}
				
			}

			//remove trailing whitespace
			int lastNonWhitespace = result.size() - 1;
			while (lastNonWhitespace >= 0 && result[lastNonWhitespace] == ' ') {
				--lastNonWhitespace;
			}
			result = result.substr(0, lastNonWhitespace + 1);

			return result;
		}


	


		private:


			list<Poi> filterPoi(list<Poi> pois, vector<string> searchTokens) {


				list<Poi> arrStart;
				list<Poi> arrOtherWordStart;
				list<Poi> arrContaining;
				list<Poi> arrDesc;
				list<Poi> arrKeywords;
				list<Poi> arrType;
				list<Poi> arrMain;
				bool found;


				for (Poi poi : pois) {

					//if trigger poi, remove from the arr main.

					string poiType = poi.getType();

					if (poiType == "displayarea" || poiType == "trigger" || poiType == "externaltrigger") {


						if (contains(arrMain, poi)) {

							cout << "here" << endl;

							arrMain.remove(poi);

						}

					}


				}


				for (int i = 0; i < searchTokens.size(); i++) {


					string token = searchTokens[i];


					arrStart.clear();
					arrOtherWordStart.clear();
					arrContaining.clear();

					for (Poi poi : pois) {


						//check name
						
						if (poi.getSearchName().size() > 0) {

							string name = poi.getSearchName();

							name.erase(remove (name.begin(), name.end(), '\''), name.end());
							name.erase(remove(name.begin(), name.end(), '&'), name.end());
							name.erase(remove(name.begin(), name.end(), ' '), name.end());

							// 1.1 If name begins with token
							if (startsWith(name, token)) {
								if (contains(arrMain, poi)) {
									arrMain.remove(poi);
									arrMain.insert(arrMain.begin(), poi);
									continue;
								}
								arrStart.push_back(poi);
								continue;

							}

							//1.2 If the other words of the name begins with token
							if (name.find(" " + token) != std::string::npos) {
								if (contains(arrMain, poi)) {
									arrMain.remove(poi);
									arrMain.insert(arrMain.begin(), poi);
									continue;
								}

								arrOtherWordStart.push_back(poi);
								continue;

							}
							

							//1.3 If name contains token
							if (name.find(token) != std::string::npos) {
								if (contains(arrMain, poi)) {
									arrMain.remove(poi);
									arrMain.insert(arrMain.begin(), poi);
									continue;
								}

								arrContaining.push_back(poi);
								continue;

							}

						}

						// If the search string has multiple words,
						// first element of searchtokens array is search string itself
						//For the first element, check only the name, discard rest
						if (searchTokens.size() > 1 && i == 0) {

							continue;

						}

						
						//2- If token is contained in description

						if (poi.getSearchDescription().size() > 0) {
							string desc = poi.getSearchDescription();
							if (desc.find(token) != std::string::npos) {
								arrDesc.push_back(poi);
								continue;
							}
						}


						//3- If token is contained in type
						

						if(poi.getSearchType().size() > 0) {
							string type = poi.getSearchType();
							if (type.find(token) != std::string::npos) {
								arrType.push_back(poi);
								continue;
							}
							if(searchTokens.size()>1){
								string adjoinedToken = searchTokens[0];
								adjoinedToken.erase(remove(adjoinedToken.begin(), adjoinedToken.end(), '-'), adjoinedToken.end());
								adjoinedToken.erase(remove(adjoinedToken.begin(), adjoinedToken.end(), ' '), adjoinedToken.end());

							
								if (poi.getSearchType().find(adjoinedToken) != std::string::npos) {
									arrType.push_back(poi);
									continue;
								}
							}
				

						}


						//4- If token is contained among keywords

						list<string> *ptr = &poi.getSearchKeywords();

						

						if (ptr != NULL) {

							for (string keyword : poi.getSearchKeywords()) {

								if (keyword.length() == 0) {
									continue;
								}
								if (keyword.find(token) != std::string::npos) {
									arrKeywords.push_back(poi);
									break;
								}
							}
						}
					}

					//Add name founds to main array after every token.
					arrMain = combineListsWithoutDuplicates(arrStart, arrMain);
					arrMain = combineListsWithoutDuplicates(arrOtherWordStart, arrMain);
					arrMain = combineListsWithoutDuplicates(arrContaining, arrMain);

				}
					
				// Add other founds to main array.
				arrMain = combineListsWithoutDuplicates(arrDesc, arrMain);
				arrMain = combineListsWithoutDuplicates(arrType, arrMain);
				arrMain = combineListsWithoutDuplicates(arrKeywords, arrMain);

				return arrMain;
			}



	private: 
		list<Poi> combineListsWithoutDuplicates(list<Poi> addition, list<Poi> original) {
				for (Poi poi : addition) {
					if (!contains(original, poi)) {
						original.push_back(poi);
					}
				}
				return original;
			}






			bool startsWith(string mainStr, string toMatch)
			{
				// std::string::find returns 0 if toMatch is found at starting
				if (mainStr.find(toMatch) == 0)
					return true;
				else
					return false;
			}


			bool contains(const std::list<Poi> &list, Poi x)
			{

				for (Poi search : list) {

					if (search.getFacilityId() == x.getFacilityId())
						return true;

				}
				return false;

			}



};




int main() {



	PoiManagerImpl manager; 

	Poi a(3, "trigger", 6 ," mvnsdkjjddj" );

	Poi b(3, "shop", 8, "dwqdfreg");

	Poi c(4, "entertainment", 9, "dedewjje" );

	Poi d(3, "food", 6 ,"dedqweje");

	Poi e(3, "food", 6 ,"qweidkj");

	Poi f(8, "entertainment", 1 ,"nwerbandkwej");

	list<Poi> lst;

	lst.push_back(a);
	lst.push_back(b);
	lst.push_back(c);
	lst.push_back(d);
	lst.push_back(e);
	lst.push_back(f);


	//second data with different key commended below


	Poi a2(1, "displayarea", 2 ,"kwjfbkj");

	Poi b2(7, "shop", 3,"dededwed");

	Poi c2(5, "entertainment", 3 ,"dedeadsdew");

	Poi d2(5, "food", 5,"qewryt");

	Poi e2(4, "food", 2, " xmvnbm");

	Poi f2(6, "entertainment", 9 ,"sqjhdjwegf");

	lst.clear();

	lst.push_back(a2);
	lst.push_back(b2);
	lst.push_back(c2);
	lst.push_back(d2);
	lst.push_back(e2);
	lst.push_back(f2);



	manager.groupedPois.insert (pair<string, list<Poi>>("key1", lst));

	manager.groupedPois.insert(pair<string, list<Poi>>("key2", lst));

	vector<string> query = { "shop" , "entertainment" };
	
	

	PoiContainer cont = manager.getAllPoi();
	
	

	//manager.printPoiList(cont);

	




	

	cin.get();

}